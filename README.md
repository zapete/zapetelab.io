# Zapete

A simple web socket-based remote control for your desktop PC. right now it only sends mplayer commands, but if I can be bothered maybe I'll implement custom buttons / commands.

## Installation

``` ejs
cd gitlab.zapete.io
npm start
```

If you find one of the [MobilOhm](https://mobilohm.gitlab.io/) apps helpful and would like to support its development, consider making a contribution through [Ko-fi](https://ko-fi.com/yphil) or [Liberapay](https://liberapay.com/yPhil/).

Your support helps keep this app free, open source, and ad-free.
